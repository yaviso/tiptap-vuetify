import AbstractExtensionInterface from "../extensions/AbstractExtensionInterface";

export default abstract class AbstractExtension
  implements AbstractExtensionInterface {
  nativeExtensionInstance = null;

  /**
   * Доступные для отображения кнопки (например в зависимости от настроек)
   */
  abstract get availableActions(): any;

  protected constructor(protected options: any, protected extensionClass: any) {
    if (extensionClass) {
      // eslint-disable-next-line
      this.nativeExtensionInstance = new extensionClass(options);
    }
  }
}
