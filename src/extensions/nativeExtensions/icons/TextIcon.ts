import IconInterface from "./IconInterface";

export default class TextIcon implements IconInterface {
  constructor(public text: any) {}

  toString() {
    return this.text;
  }
}
