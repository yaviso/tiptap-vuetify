import { TableHeader as TableHeaderOriginal } from "tiptap-extensions";
import ExtensionActionInterface from "../../actions/ExtensionActionInterface";
import AbstractExtension from "../../AbstractExtension";

export default class TableHeader extends AbstractExtension {
  constructor(options: any) {
    super(options, TableHeaderOriginal);
  }

  get availableActions(): ExtensionActionInterface[] {
    return [];
  }
}
