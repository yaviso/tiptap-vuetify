declare module "tiptap" {
  import { Vue } from "vue/types/vue";

  // const _a: any
  // export = _a
  // import * as Tiptap from 'tiptap'
  // export * from 'tiptap'
  // // export { Extension } from 'tiptap'
  // export type Extension = any
  // export type Editor = any
  // export type EditorMenuBubble = any
  export class Extension {
    constructor(...arg: any[]);
    [key: string]: any;
  }
  export class Editor {
    constructor(...arg: any[]);
    [key: string]: any;
  }
  export class EditorMenuBubble extends Vue {
    [key: string]: any;
  }
  export class EditorContent extends Vue {
    [key: string]: any;
  }
  export class EditorMenuBar extends Vue {
    [key: string]: any;
  }

  // Heading,
  //   Bold,
  //   Italic,
  //   Strike,
  //   Underline,
  //   Code,
  //   CodeBlock,
  //   Paragraph,
  //   BulletList,
  //   OrderedList,
  //   ListItem,
  //   Blockquote,
  //   HardBreak,
  //   HorizontalRule,
  //   History,
  //   Link
}

declare module "tiptap-extensions" {
  export class Blockquote {}
  export class Bold {}
  export class BulletList {}
  export class Code {}
  export class CodeBlock {}
  export class HardBreak {}
  export class Heading {}
  export class History {}
  export class HorizontalRule {}
  export class Image {}
  export class Italic {}
  export class Link {}
  export class ListItem {}
  export class OrderedList {}
  export class Placeholder {
    constructor(options: any);
  }
  export class Strike {}
  export class Table {}
  export class TableCell {}
  export class TableHeader {}
  export class TableRow {}
  export class TodoList {}
  export class Underline {}
}

declare module "tiptap-commands" {
  export const sinkListItem: Function;
  export const splitToDefaultListItem: Function;
  export const liftListItem: Function;
}
